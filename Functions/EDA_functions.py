import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


class ExploratoryDataAnalysis:
    
    '''
    The Class ExploratoryDataAnlysis

    Attributes
    ----------

    Methods
    ----------

    '''
    def __int__(self, dataset=None):
        self.dataset = dataset
    
    def missing_values(self, dataset):
        """
        Generates a dataframe with the number 
        and percentage of missing values, if they
        exist.
        
        Parameters
        ------------
        dataset
            The dataset to analyze.
        Returns
        ------------
        The table with missing values information."""
        
        nulls_per_column = [] # stores the number of nulls per column
        nulls_per_columns_percent = [] # store the percentage of nulls per 
        column_names = [] # stores the column names
    
        for column in dataset.columns:
            if dataset[column].isnull().any(): # if the column has at least one null value
                nulls_per_column.append(dataset[column].isnull().sum()) # number of nulls per column
                nulls_per_columns_percent.append(
                    str(np.around(dataset[column].isnull().sum() 
                                  / len(dataset) * 100, 3)) + "%") # percentage of nulls per column
                column_names.append(column)
        
        if np.sum(nulls_per_column) <= 0:
            print("There are no missing values in this Dataset!")
                                                 
        else:
            print("From {a} columns, there are {b} that have missing values in this dataset.".format(
                a = len(dataset.columns), b = len(column_names) )) 
            
            return pd.DataFrame(data =
                                {'Number of Nulls': nulls_per_column, 
                                 'Percentage of Nulls': nulls_per_columns_percent
                                 }, 
                                index = column_names) # returns table with the number and percentage of nulls of the inputed dataframe
        
        
    def plot_hist(self, dataset, column, title,
                  norm_term = 1, bins = 10, y_lim_sup = None):
        """
        Plots an histogram to analyze the frequency
        of the values of a column.
        
        Parameters
        -----------
        dataset
            The dataset that is subject to analysis.
        column
            The column of the dataset that is to be analyzed.
        title
            The title of the histogram.
        norm_term
            In case the variable is expressed in days, for example, 
            and you want to express it in years. Default is 1.
        bins
            Number of bins to use in the histogram.
        y_lim_sup
            The superior limit for the y-axis. Default is None.
        
        Returns
        -----------
        Histogram.
        """
        (dataset[column] / norm_term).plot.hist(
            title = title, 
            bins = bins, 
            edgecolor = 'black', 
            linewidth = 0.5, 
            alpha = 0.7, 
            figsize = (10, 5) )
        plt.ylim([0, y_lim_sup])
        plt.ticklabel_format(axis = "both", style = "plain")
        plt.show()
        
        
    def plot_pie_chart(self, dataset, column):
        """
        Plots a pie chart for categorical variables.
        
        Parameters
        -----------
        dataset
            The dataset to analyze.
        column
            The column to analyze.
            
        Returns
        -----------
        A pie chart.
        """
        
        value_count = dataset[column].value_counts() # gets the values of a column and its frequency
        sizes = []
        labels = []
        
        for ix in np.arange(len(value_count)):
            sizes.append(value_count[ix] / len(dataset[column]) * 100) # gets the frequency in percentage
            labels.append(value_count.index[ix]) # gets the labels
            
        fig, ax = plt.subplots()
        ax.pie(sizes, labels = labels, autopct='%1.2f%%', startangle = 90) # plots a pie chart with the percentage frequency of the values of a column
        ax.axis('equal')
        plt.show()
        
        
    def plot_cat_freq(self, dataset, column, title):
        """
        Plots a horizontal bar chart for the a column
        of the specified dataset.
        
        Parameters
        ------------
        dataset
            The dataset where the column is inserted.
        columns: str
            The column to analyze.
        title: str
            The title of the bar chart.
        
        Returns
        ------------
        A bar chart."""
        dataset[column].value_counts().plot(
            kind = 'barh', 
            title = title, 
            figsize = (18,6), 
            alpha = 0.7, 
            edgecolor = 'black', 
            linewidth = 0.5)
        plt.ticklabel_format(axis = "x", style = "plain")
        plt.ylabel("Frequency")
        plt.xlabel(column)
        plt.show()
        
    def count_overdue(sels, dataset):
        """
        Counts the overdue payments per id
        
        Parameters
        ------------
        dataset
            The dataset POS_CASH_balance.csv

        
        Returns
        ------------
        A dataset with the amount of overdue payments per id.
        """
        name_flag = 'OVERDUE'
        name = 'OVERDUE_ACTIVE_COUNT'
        
        def fun(x):
            over = x['SK_DPD_DEF']
            future_cnt = x['CNT_INSTALMENT_FUTURE']
                           
            if over > 0 and future_cnt > 0:
                return 1
            else:
                return 0
    
        dataset[name_flag] = dataset.apply(fun,axis=1)
        dataset[name] = dataset.groupby('SK_ID_CURR')[name_flag].transform('sum')
        dataset[name] = dataset[name].apply(lambda x:x+1)
        dataset.drop([name_flag],axis=1, inplace=True)
        
    def count_completed(self, dataset):
        """
        Count the total payments per id
        
        Parameters
        ------------
        dataset
            The dataset POS_CASH_balance.csv

        
        Returns
        ------------
        A dataset with the amount of total payments per id.
        """
        name_flag = 'COMPLETED_FLAG'
        name = 'COMPLETED_COUNT'
        dataset[name_flag] = dataset['NAME_CONTRACT_STATUS'].apply(lambda x: 1 if x =='Completed'  else 0)
        dataset[name] = dataset.groupby('SK_ID_CURR')[name_flag].transform('sum')
        dataset[name] = dataset[name].apply(lambda x:x+1)
        dataset.drop([name_flag],axis=1, inplace=True)
                
            