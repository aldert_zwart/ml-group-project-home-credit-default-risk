# -*- coding: utf-8 -*-
"""
Created on Wed Mar 31 14:10:01 2021

@author: Aldert
"""
import pandas as pd

class DataCleansing:
    '''
    The Class ExploratoryDataAnlysis

    Attributes
    ----------

    Methods
    ----------

    '''
    def __int__(self, dataset=None):
        self.dataset = dataset
        
    def missing_values_table(self, df):
        mis_val = df.isnull().sum()
        mis_val_percent = 100 * df.isnull().sum() / len(df)
        mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
        mis_val_table_ren_columns = mis_val_table.rename(
        columns = {0 : 'Missing Values', 1 : '% of Total Values'})
        mis_val_table_ren_columns = mis_val_table_ren_columns[
        mis_val_table_ren_columns.iloc[:,1] != 0].sort_values(
        '% of Total Values', ascending=False).round(1)
        return mis_val_table_ren_columns
        